# Polymer Simulation

Simulation of a two-dimensional polymer ruled by Lennard-Jones interactions. Each bead is at constant distance from its 2 nearest neighbours but the angle between these 2 can change. By Anna Akhmetyanova and Carlos Ruiz for Computational Physics, a TU Delft course.