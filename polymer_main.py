# -*- coding: utf-8 -*-
"""
POLYMER GENERATOR

by: AA & CR
"""
import numpy as np
from numpy import linalg as la

def distances(old_beads, step, angles, min_dist=0):
    """
    Calculation of distance between the new bead and the old ones for each possible angle.
    
    The function find the different posible positions of a bead(i+1) based on the bead(i)
    and compute the distances between the new bead and the beads(0 to i-1).
    
    If given a certain angle the new bead is closer than min_dist from an old bead (there 
    is overlapping) the entire row of distances and the corresponding angle are dismissed.
    
    Parameters
    ----------
    old_beads: numpy array
        Matrix with the position of the current beads.
    step: numpy array
        Matrix with all posible vectors between the last bead and the new one.
    angles: numpy array
        Posible angles of step vectors with respect the X axis.
    min_dist: float
        Minimum separation between beads.
    
    Returns:
    --------
    table: numpy array
        table[:,0] all the remaining angles which do not produce overlapping.
        table[:,1:] distances in rows between the new bead and the old ones.
    """
    new_bead = old_beads[-1,:] + step
    dist = new_bead[:,None] - old_beads[:-1,:]
    r = la.norm(dist, axis=2)
    r[r<min_dist] = 0 # putting a flag on the overlapping beads
    table = np.zeros((len(angles), dist.shape[1]+1))
    table[:,0] = angles
    table[:,1:] = r
    table = table[~np.logical_not(table).any(axis=1)]
    return table

def angle_probabilities(r_matrix, temperature, interaction=True):
    """
    Calculation of the probabilities of each angle given the energy associated.
    
    Parameters:
    ----------
    r_matrix: numpy array
        The distances between the new bead and the old beads for every possibly angle.
    temperature: float
        The temperature of the system in natural units, i. e. temperature = kT in units of epsilon.
    interaction: boolean
        Presence or absence of interaction between beads.
        
    Returns:
    --------
    prob: numpy array
        Probabilities of the different angles.
    W: float
        The weight factor of the current iteration.
    """
    if interaction == False:
        prob = np.ones(r_matrix.shape[0])/r_matrix.shape[0]
        W = 1
    else:
        r = r_matrix.copy()
        r_inv6 = r**(-6)
        pots = 4*r_inv6*(r_inv6 - 1)
        total = pots.sum(axis=1)
        total -= np.amin(total)
        prob = np.exp(-total/temperature)
        W = np.sum(prob)
        prob = prob/W
    return prob, W

def end2end_distance(positions_matrix, weights):
    """
    Calculation of the weighted average of the end-to-end distance over the ensemble.
    
    Parameters:
    -----------
    positions_matrix: numpy array
        The positions of the beads of the polymers.
    weights: numpy array
        The weight factors of the polymers.
        
    Returns:
    -------
    R_mean: float
        The weighted average end-to-end distance.
    error: float
        The standart deviation of the end-to-end distance.
    """
    
    M = positions_matrix.shape[0] #number of polymers
    R = la.norm(positions_matrix[:,-1,:], axis = 1) #end-to-end distance
    R_mean = np.sum(R*weights)/np.sum(weights)
    w_mean = np.mean(weights)
    error2 = (M/((M-1)*np.sum(weights)**2))*(np.sum((weights*R-w_mean*R_mean)**2) - 2*R_mean*np.sum((weights-w_mean)*(weights*R-w_mean*R_mean)) + R_mean**2*np.sum((weights-w_mean)**2))
    error = np.sqrt(error2)

    return R_mean, error

def generator_rosenbluth(beads, polymers, intervals, temperature, interaction=True, min_dist=0, unit_length=1):
    """
    Generation of an ensemble of polymers based on the functions distances and probabilities.
    
    Parameters:
    ----------
    beads: list of integers
        Different number of beads from which we want to extract information.
    polymers: integer
        Number of polymers in the ensemble.
    intervals: integer
        Number of different rotations each new bead can take with respect the last one.
    temperature: float
        Temperature of the system in natural units, i. e. temperature = kT in units of epsilon.
    interaction: boolean
        Presence or absence of interaction between beads.
    min_dist: float
        Minimum distance between beads.
    unit_length: float
        The distance between adjacent beads.
    
    Returns:
    --------
    positions: numpy array
        Positions of the beads of each polymer: [polymer_number, bead_number, coordinate].
    rotations: numpy array
        Chosen (N-2) angles and corresponding probabilities for each polymer:
            rotations[polymer_number,:,0] N-2 chosen angles.
            rotations[polymer_number,:,1] probabilities associated to the chosen angles.
    data: numpy array
        Table with the weighted average end to end distances for each number of bead with the error:
            data[:,0] number of beads.
            data[:,1] weighted average of the end to end distance.
            data[:,2] uncertainty of the end to end distance.
    """
    positions = np.zeros((polymers,beads[-1],2))
    positions[:,1,0] = unit_length
    rotations = np.zeros((polymers,beads[-1]-2,2))
    data = np.zeros((len(beads),3))
    data[:,0] = beads
    
    weights = np.zeros(polymers) 
    W = np.zeros((polymers,beads[-1]-2))
    
    angles = np.arange(0,2*np.pi,2*np.pi/intervals)
    step = np.zeros((intervals,2))
    step[:,0] = np.cos(angles)*unit_length
    step[:,1] = np.sin(angles)*unit_length
    
    for j in range(polymers):
        for i in range(beads[-1]-2):
            angles += np.random.random()*2*np.pi/intervals
            table = distances(positions[j,:i+2,:], step, angles, min_dist)
            choices = table[:,0]
            probs, W[j,i] = angle_probabilities(table[:,1:], temperature, interaction)
            rotations[j,i,0] = np.random.choice(choices, 1, p=probs)
            flag = np.sum(np.where(choices == rotations[j,i,0]))
            rotations[j,i,1] = probs[flag]
            positions[j,i+2,0] = positions[j,i+1,0] + np.cos(rotations[j,i,0])*unit_length
            positions[j,i+2,1] = positions[j,i+1,1] + np.sin(rotations[j,i,0])*unit_length     
    W = np.log(W)
    
    for n in beads:
        weights = np.sum(W[:,:(n-2)], axis = 1)
        weights -= np.amax(weights)
        weights = np.exp(weights)
        data[np.where(data[:,0] == n),1], data[np.where(data[:,0] == n),2] = end2end_distance(positions[:,:n,:], weights)

    return positions, rotations, data
        

def generator_PERM(beads, polymers, intervals, temperature, ep_gap, alpha_up=2.0, alpha_low=1.2, unit_length=1, min_dist=0, interaction=True):
    """
    Generation of an ensemble of polymers based on the functions distances and probabilities using PERM.
    
    Parameters:
    ----------
    beads: list of integers.
        Different number of beads from which we want to extract information.
    polymers: integer
        Number of polymers in the initial ensemble.
    intervals: integer
        Number of different rotations each new bead can take with respect the last one.
    temperature: float
        Temperature of the system in natural units, i. e. temperature = kT in units of epsilon.
    ep_gap: integer
        Number of steps between the enriching-pruning procedures. (The enriching-pruning is applied after every k-th step.)
    alpha_up: float
        Coefficient of the upper limit for enriching (if applicable).
    alpha_low: float
        Coefficient of the lower limit for pruning (if applicable).
    unit_length: float
        The distance between adjacent beads.
    min_dist: float
        Minimum distance between beads.
    interaction: boolean
        Presence or absence of interaction between beads.
    
    Returns:
    --------
    positions: numpy array
        Positions of the beads of each polymer: [polymer_number, bead_number, coordinate].
    rotations: numpy array
        Chosen (N-2) angles and corresponding probabilities for each polymer:
            rotations[polymer_number,:,0] N-2 chosen angles.
            rotations[polymer_number,:,1] probabilities associated to the chosen angles.
    data: numpy array
        Table with the weighted average end to end distances for each number of bead with the error:
            data[:,0] number of beads.
            data[:,1] weighted average of the end to end distance.
            data[:,2] uncertainty of the end to end distance.        
    """
    positions = np.zeros((polymers,beads[-1],2))
    rotations = np.zeros((polymers,beads[-1]-2,2))
    weights = np.ones(polymers)   
    positions[:,1,0] = unit_length
    data = np.ones((len(beads),3))
    data[:,0] = beads
    
    angles = np.arange(0,2*np.pi,2*np.pi/intervals)
    step = np.zeros((intervals,2))
    step[:,0] = np.cos(angles)*unit_length
    step[:,1] = np.sin(angles)*unit_length
    
    for i in range(beads[-1]-2):
        polymers = positions.shape[0]
        for j in range(polymers):
            angles += np.random.random()*2*np.pi/intervals
            table = distances(positions[j,:i+2,:], step, angles, min_dist)
            choices = table[:,0]
            probs, W = angle_probabilities(table[:,1:], temperature, interaction)
            weights[j] *= (W)
            rotations[j,i,0] = np.random.choice(choices, 1, p=probs)
            flag = np.sum(np.where(choices == rotations[j,i,0]))
            rotations[j,i,1] = probs[flag]          
            positions[j,i+2,0] = positions[j,i+1,0] + np.cos(rotations[j,i,0])*unit_length
            positions[j,i+2,1] = positions[j,i+1,1] + np.sin(rotations[j,i,0])*unit_length
        if (i+3) in beads:
            print('beads ', (i+3), 'polymers ', polymers)
            weights_c = np.log(weights)
            weights_c -= np.amax(weights_c)
            weights_c = np.exp(weights_c)
            data[np.where(data[:,0] == i+3),1], data[np.where(data[:,0] == i+3),2] = end2end_distance(positions[:,:(i+3),:], weights_c)
        if i % ep_gap == 0:
            #"""
            frac_up = np.int(polymers*0.05)
            frac_low = np.int(polymers*0.1)
            best = np.argpartition(weights, -frac_up)[-frac_up:]
            worst = np.argpartition(weights, frac_low)[:frac_low]
            #enriching
            positions = np.append(positions, positions[best,:,:], axis = 0)
            rotations = np.append(rotations, rotations[best,:,:], axis = 0)
            weights = np.append(weights, weights[best]/2, axis = 0)
            weights[best] = weights[best]/2 
            #pruning
            R = np.random.rand(frac_low)
            delete = worst[np.where(R < 0.5)]
            keep = worst[np.where(R >= 0.5)]
            weights[keep] *= 2
            positions = np.delete(positions, delete, axis = 0)
            rotations = np.delete(rotations, delete, axis = 0)
            weights = np.delete(weights, delete, axis = 0)
            """
            Alternative procedure of enriching/pruning using the upper and lower limits.
            average_weight = np.mean(weights)
            up_limit = alpha_up*average_weight
            low_limit = alpha_low*average_weight           
            for j in range(polymers):
                #enriching
                if weights[j] > up_limit:
                    positions = np.append(positions, positions[[j],:,:], axis = 0)
                    rotations = np.append(rotations, rotations[[j],:,:], axis = 0)
                    weights = np.append(weights, weights[[j]]/2, axis = 0)
                    weights[j] = weights[j]/2                
                #pruning
                if weights[j] < low_limit:
                    R = np.random.random()
                    if R < 0.5:
                        weights[j] = 0
                    else:
                        weights[j] *= 2
            delete = np.where(weights == 0)
            positions = np.delete(positions, delete, axis = 0)
            rotations = np.delete(rotations, delete, axis = 0)
            weights = np.delete(weights, delete, axis = 0)
            """
    
    return positions, rotations, data

